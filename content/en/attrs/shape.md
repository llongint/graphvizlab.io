---
defaults:
- ellipse
flags: []
minimums: []
name: shape
types:
- shape
used_by: "N"
---
Sets the [shape](/doc/info/shapes.html) of a node.
