---
name: JPEG
params:
- jpg
- jpeg
- jpe
---
Output JPEG compressed image files.
