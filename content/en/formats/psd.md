---
name: PSD
params:
- psd
---
Output in the Adobe PhotoShop PSD file format.
